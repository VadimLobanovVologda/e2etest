const { firefox } = require("playwright");

test("e2e test", async () => {
  const browser = await firefox.launch({
    headless: true,
    slowMo: 100,
    args: ["--window-size=1920,1080"]
  });
  const context = await browser.newContext();
  const page = await context.newPage();
  await page.goto("http://yandex.ru/");
  const dimensions = await page.evaluate(() => ({
    width: document.documentElement.clientWidth,
    height: document.documentElement.clientHeight,
    deviceScaleFactor: window.devicePixelRatio
  }));
  console.log(dimensions);

  await browser.close();
  expect(1).toBe(1);
}, 20000);
